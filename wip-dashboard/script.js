function myFunction() {
  var data = [];
  for (var page = 1; page < 10; page++) {
    var response = UrlFetchApp.fetch("https://gitlab.com/api/v4/groups/gitlab-org/issues?state=opened&per_page=100&labels=devops::release,Deliverable&page=" + page);
    // Parse the JSON reply
    var json = response.getContentText();
    data = data.concat(JSON.parse(json));
    if (json.length < 100 ) {
      break;
    }
  }

  function getRelatedMRs(projectId, issueId){
    var response = UrlFetchApp.fetch("https://gitlab.com/api/v4/projects/" + projectId + "/issues/" + issueId + "/related_merge_requests");
    var json = response.getContentText();

    return JSON.parse(json);
  }

  function getWoflowStage(labels){
    if(labels.match(/workflow::start/i)){
      return "Start"
    } else if(labels.match(/workflow::problem validation/i)){
      return "Problem Validation"
    } else if(labels.match(/workflow::solution validation/i)){
      return "Solution Validation"
    } else if(labels.match(/workflow::ready for development/i)){
      return "Ready for Development"
    } else if(labels.match(/workflow::in dev/i)){
      return "In Dev"
    } else if(labels.match(/workflow::in review/i)){
      return "In Review"
    } else if(labels.match(/workflow::verification/i)){
      return "Verification"
    } else {
      return ""
    }
  }

  function getFeature(labels){
    if(labels.match(/pages/i)){
      return "Pages"
    } else if(labels.match(/release orchestration/i)){
      return "Release Orchestration"
    } else if(labels.match(/merge trains/i)){
      return "Merge Trains"
    } else if(labels.match(/feature flags/i)){
      return "Feature Flags"
    } else {
      return ""
    }
  }

  function formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
  }

  function getMilestone(milestone_object){
    if(milestone_object){
      return milestone_object["title"]
    } else {
      return ""
    }
  }

  function getAuthor(author){
    if(author){
      return author["name"]
    } else {
      return ""
    }
  }

  function getAassignees(assignees){
    var assigneeNames = [];
    assignees.forEach(function(assignee,i) {
      assigneeNames.push(assignee["name"])
    })

    return assigneeNames.join(',')
  }

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();

  var output = [];
  var columns = [
    "Title",
    "URL",
    "Author",
    "Assignees",
    "Created At",
    "Days Old",
    "Workflow Stage",
    "Milestone",
    "Feature",
    "Labels",
    "",
    "Issue or MR"
  ]
  output.push(columns);

  data.forEach(function(issue,i) {
    output.push(
      [
        issue["title"],
        issue["web_url"],
        getAuthor(issue["author"]),
        getAassignees(issue["assignees"]),
        formatDate(issue["created_at"]),
        "=TODAY()-E" + (output.length + 1),
        getWoflowStage(String(issue["labels"])),
        getMilestone(issue["milestone"]),
        getFeature(String(issue["labels"])),
        issue["labels"].join(),
        "",
        "issue"
      ]
    );

    var relatedMRs = getRelatedMRs(issue["project_id"], issue["iid"])

// think about adding days since last activity
// think about a way to account for stretch labels
// figure out how to pull confidential issues
// this issue is missing for some reason? https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/32127



    relatedMRs.forEach(function(mr, j){
      if(mr['state'] == "opened"){
        output.push(
          [
            mr["title"],
            mr["web_url"],
            getAuthor(mr["author"]),
            getAassignees(mr["assignees"]),
            formatDate(mr["created_at"]),
            "=TODAY()-E" + (output.length + 1),
            getWoflowStage(String(mr["labels"])),
            getMilestone(mr["milestone"]),
            getFeature(String(mr["labels"])),
            mr["labels"].join(),
            '',
            'mr'
          ]
        );
      }
    })
  });

  var len = output.length;

  // clear any previous content
  sheet.getRange(1,1,500,columns.length).clearContent();

  // paste in the values
  sheet.getRange(1,1,len,columns.length).setValues(output);
}
